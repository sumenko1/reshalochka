from random import randint, randrange, choice


def generate_sum(min, max, count):
    calc = []
    for i, n in enumerate(range(count)):
        a = sorted((randint(min, max), randint(min, max)), reverse=True)
        
        sign = choice(('-', '+'))
        if sign == '-':
            answer = a[0] - a[1]
        else:
            answer = a[0] + a[1]

        calc += [a[0], a[1], sign, answer], 
        # print(f'{i+1:<3}:  {a[0]:3} {sign} {a[1]:3} = ___')
    return calc


if __name__ == '__main__':
    a = generate_sum(1, 100, 2)
    print(a)
