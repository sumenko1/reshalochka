from flask import Flask, render_template, request, redirect, url_for
from kidMath import generate_sum
app = Flask(__name__)

min_int = 1
max_int = 50
number = 2
tasks = []

@app.template_filter('set_width')
def set_width_filter(s):
    return str(s).rjust(4, ' ')

@app.route('/new')
@app.route('/new/<set_number>')
def new_task(set_number=2):
    global tasks, number

    if int(set_number) == 0:
        new_task()
    else:
        number = int(set_number)

    tasks_new = generate_sum(min_int, max_int, number)

    tasks = []
    for id, task in enumerate(tasks_new):
        task_dict = {
            'id': id + 1,
            'value_a':  task[0],
            'value_b':  task[1],
            'sign':     task[2],
            'correct':   task[3],
            'answer':   '',
            'msg':      ''
        }
        tasks.append(task_dict)

    return redirect(url_for('index'))


@app.route('/', methods = ['POST', 'GET'])
def index():
    global tasks
    total_msg = ''
    if len(tasks) == 0:
        new_task()

    if request.method == 'GET':
        return render_template('math.html', tasks=tasks)
    else:
        form = request.form
        correct_answers = 0
        empty = 0
        next = False
        # https://getbootstrap.com/docs/5.1/components
        for i in range(number):
            answer = form[f'answer-{i+1}']
            if answer == '':
                empty += 1
            tasks[i]['answer'] = answer
            if answer != '' and tasks[i]['correct'] == int(answer):
                tasks[i]['msg'] = '+'
                correct_answers += 1
            else:
                tasks[i]['msg'] = '-'
        
        if number == correct_answers:
            total_msg = 'Все верно!'
            next = True
        elif empty == number:
            total_msg = ''
        else:
            total_msg = 'Есть ошибки'

        return render_template('math.html', tasks=tasks, total_msg=total_msg, next = next)


if __name__ == '__main__':
    app.run(debug=True,
            host='0.0.0.0',
            port=555)
